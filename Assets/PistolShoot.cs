﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PistolShoot : NetworkBehaviour
{

	//TODO: Need to make this generic for other weapons.

	//NOTE: PLAYER SHOOT dictates if shot fired. This means that shots fired, even when animations playing.

	GameObject ch;
	//what is ch??? Is this PlayerWeapon?
	Animation gunAnim;
	AudioSource gunSound;
	bool isFiring = false;
	bool isADS = false;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (!gunAnim.isPlaying) {
			Debug.Log ("ANIMATION IS NOT PLAYING");
			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
				isFiring = true;
			} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
				isFiring = false;
			}

			List<string> gunAnimNames = new List<string> ();
			foreach (AnimationState state in gunAnim) {
				gunAnimNames.Add (state.name);
			}


			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && GlobalAmmo.currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADSFire");
				} else {
					gunAnim.Play ("Hipfire");
				}
				gunSound.Play ();
				GlobalAmmo.currentAmmo--;

			}
		}
	}
}
