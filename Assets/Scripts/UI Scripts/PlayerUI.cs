﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Text))]
public class PlayerUI : MonoBehaviour {
	[SerializeField]
	RectTransform playerHealthBarFill;
	[SerializeField]
	RectTransform baseHealthBarFill;
	[SerializeField]
	Text txtFunds;
	[SerializeField]
	Text txtAmmo;
	[SerializeField]
	Text txtPhase;
	[SerializeField]
	Text txtGameOver;
	[SerializeField]
	Text txtTowerType;
	[SerializeField]
	Text txtTowerPrice;

	Base b;
	GridInteraction gi;
	private GameObject player;
	public void SetPlayer(GameObject value) {
		gi = value.GetComponent<GridInteraction> ();
		player = value; 
	}

	void Start(){
		b = GameObject.FindGameObjectWithTag ("Base").GetComponent<Base>();
	}
	void Update(){
		SetBaseHealthBar (b.currentHealth);
		SetPhaseTxt (GameManager.state, (int)GameManager.nextPhaseCountdown);
		SetPlayerFundsTxt(player.GetComponent<Player>().funds);
		if (gi != null) {
			Tower tower = gi.currentTower;
			if (tower != null) {
				SetTowerTxt (tower.price, tower.type);
			}
		}
	}
	private void SetPlayerFundsTxt(int _funds){
		txtFunds.text = _funds.ToString();
	}
	public void SetPlayerHealthBar(float _amount){
		playerHealthBarFill.localScale = new Vector3 (_amount, 1f, 1f);
		Debug.Log ("bar " + _amount);
	}
	private void SetBaseHealthBar(float _amount){
		playerHealthBarFill.localScale = new Vector3 (_amount, 1f, 1f);
	}
	private void SetPhaseTxt(GameManager.GameState  _state, int _countdown){
		if (_state == GameManager.GameState.PHASE_BUILDING) {
			txtPhase.text = "Building: " + _countdown + "s";
		} else if (_state == GameManager.GameState.PHASE_FIGHTING) {
			txtPhase.text = "Battle!";
		}
	}
	private void SetTowerTxt(int _price, string _type){
		txtTowerPrice.text = "- $" + _price;
		txtTowerType.text = _type;
	}
}
